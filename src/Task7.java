public class Task7 {


    public static class ListRealisation<T> {

        private int firstSize;
        private int size = 0;
        private Object[] array;

        public ListRealisation(int firstSize) {
            this.firstSize = firstSize;
            array = new Object[firstSize];
        }
        public ListRealisation() {
            this.firstSize = 5;
            array = new Object[firstSize];
        }



        public void add(T element) {

            newSizeIfNecessary();
            array[size++] = element;

        }


        public void newSizeIfNecessary() {
            if (size == array.length) {
                Object [] newArray = new Object[array.length * 2];
                System.arraycopy(array, 0, newArray, 0, size);
                array = newArray;

            }
        }

        public T remove(int index) {
            T remEl = (T) array[index];
            System.arraycopy(array, index + 1, array, index, array.length - index - 1);
            size--;
            return remEl;
        }

        public boolean removeElement(T element) {

            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(element)) {
                    remove(i);
                    return true;
                }

            }
            return false;
        }


        public boolean contains(T element) {
            for (int i = 0; i < size; i++) {
                if (array[i].equals(element)) {
                    return true;
                }
            }
            return false;
        }

        public int size() {
            return size;
        }

        public boolean isEmpty() {
            if (size == 0) {
                return true;
            } else {
                return false;
            }
        }


        public T get(int index) {
            return (T) array[index];
        }


        public void clear() {
            array = new Object[firstSize];
            size = 0;
        }


    }


    public static void main(String[] args) {

        ListRealisation<Integer> listArray = new ListRealisation<>();
        listArray.add(11);
        listArray.add(11);
        listArray.add(11);
        listArray.add(11);
        listArray.add(11);
        listArray.add(66);
        listArray.add(77);
        listArray.add(88);
        listArray.add(99);
        listArray.add(100);
        listArray.add(200);
        listArray.add(300);
        listArray.add(400);

        //add
        for (int i = 0; i < listArray.array.length; i++) {
            System.out.println(listArray.get(i));
        }
        System.out.println();
        System.out.println(listArray.array.length + " array length");
        System.out.println();

        //remove index
        listArray.remove(2);

        for (int i = 0; i < listArray.array.length; i++) {
            System.out.println(listArray.get(i));
        }

        System.out.println();
        System.out.println();
        System.out.println(listArray.array.length + " array length");

        System.out.println();
        System.out.println();

        //remove element

        listArray.removeElement(200);
        for (Object o : listArray.array) {
            System.out.println(o);
        }
        System.out.println();
        System.out.println();
        System.out.println(listArray.array.length + " array length");
        System.out.println();
        System.out.println();

        //contains
        System.out.println(listArray.contains(11));//true
        System.out.println(listArray.contains(5));//false
        System.out.println();
        System.out.println();

        //size
        System.out.println(listArray.size());
        System.out.println();
        System.out.println();

        //isEmpty
        System.out.println(listArray.isEmpty());
        System.out.println();
        System.out.println();

        //get
        System.out.println(listArray.get(5)); //77
        System.out.println();
        System.out.println();

        //clear
        listArray.clear();
        System.out.println(listArray.size()); //0


    }
}